#include <iostream>
#include <string>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <cassert>
#include <stdlib.h>
#include <stdarg.h>
#include <iomanip>

using namespace std;

class Color {
public:
    unsigned char rgb[3];

    Color() {
        rgb[0] = 255;
        rgb[1] = 128;
        rgb[2] = 128;
    }

    Color(unsigned char _r, unsigned char _g, unsigned char _b) {
        rgb[0] = _r;
        rgb[1] = _g;
        rgb[2] = _b;
    }

    string get_hex(int index) {
        string hex;

        //00001111
        char mask_min = 15;
        char left_hex = mask_min & rgb[index];
        
        char right_hex = rgb[index] >> 4;
        right_hex = mask_min & right_hex;
        
        switch(left_hex) {
            case 10: left_hex = 'a'; break;
            case 11: left_hex = 'b'; break;
            case 12: left_hex = 'c'; break;
            case 13: left_hex = 'd'; break;
            case 14: left_hex = 'e'; break;
            case 15: left_hex = 'f'; break;
            default: left_hex = '0' + left_hex; break;
        }
        switch(right_hex) {
            case 10: right_hex = 'a'; break;
            case 11: right_hex = 'b'; break;
            case 12: right_hex = 'c'; break;
            case 13: right_hex = 'd'; break;
            case 14: right_hex = 'e'; break;
            case 15: right_hex = 'f'; break;
            default: right_hex = '0' + right_hex; break;
        }

        hex.push_back(right_hex);
        hex.push_back(left_hex);

        return hex;
    }

    string get_col() const {
        string col = "\e[48;2;";
        col.append(to_string(rgb[0]));
        col.push_back(';');
        col.append(to_string(rgb[1]));
        col.push_back(';');
        col.append(to_string(rgb[2]));
        col.push_back('m');

        return col;
    }

    string get_slider(int index) const {
        float ratio = rgb[index] / 255.f;
        int slider_length = ratio * 25;

        Color col(0, 0, 0);
        col.rgb[index] = rgb[index];

        string slider = "[";
        slider.append(col.get_col());
        for(int i = 0; i < slider_length; i++) {
            slider.push_back(' ');
        }
        slider.append("\e[0m");
        for(int i = slider_length; i < 25; i++) {
            slider.push_back('-');
        }
        slider.push_back(']');
        
        return slider;
    }

    string get_colored_square() const {
        string square = "";
        for(int i = 0; i < 3; i++) {
            square.append("  " + get_col());
            for(int j = 0; j < 8; j++) {
                square.push_back(' ');
            }
            square.append("\e[0m\n");
        }
        
        return square;
    }
};


char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}

void handle_input(bool& is_running, int& selected_row, Color& col) {
    
    char c = getch();

        switch(c) {
            case 27:
                is_running = false;
                break;
            case 'z':
                selected_row > 0 ?
                    selected_row--
                :   selected_row = 2;
                break;
            case 's':
                selected_row < 2 ?
                    selected_row++
                :   selected_row = 0;
                break;
            case 'd':
                col.rgb[selected_row]++;
                break;
            case 'q':
                col.rgb[selected_row]--;
                break;

        }

}

void print_screen(const Color& col, const int& selected_row) {
    for(int i = 0; i < 3; i++) {
        cout << (selected_row == i ? '>' : ' ') << col.get_slider(i) << " " << (int)col.rgb[i] << "   " << endl;
    }
    cout << endl;
    cout << col.get_colored_square() << endl;
    cout << "\e[8A";
}

int main(void) {
    Color col;
    bool is_running = true;
    int selected_row = 0;

    while(is_running) {
        print_screen(col, selected_row);
        handle_input(is_running, selected_row, col);
    }

    cout << "\e[0J";
    cout << endl;
    cout << "Code rgb: " << col.get_col() << "  " << "\e[0m" << " " << setprecision(2) << (int)col.rgb[0] << " " << (int)col.rgb[1] << " " << (int)col.rgb[2] << endl;
    cout << "   float: " << col.get_col() << "  " << "\e[0m" << " " << col.rgb[0] / 255.0 << " " << col.rgb[1] / 255.0 << " " << col.rgb[2] / 255.0 << endl;
    cout << "Code hex: " << col.get_col() << "  " << "\e[0m" << " " << col.get_hex(0) << col.get_hex(1) << col.get_hex(2) << endl;
    cout << endl;
    return 0;
}
