% color-picker(1) color-picker 1.0.0
% Noah Bertholon
% May 2022

# NAME
color-picker - color picker tool in the command prompt

# SYNOPSIS
**color-picker**

# DESCRIPTION
**color-picker** Print a color picker tool. z (up), s (down) to navigate through color channel. q (decrease), d (increase) to modify channel value.\
**st-graal** https://prirai.github.io/posts/ansi-esc/ 
